# kali-greenbone
This repository contains a Vagrant definition for setting up Kali and
installing Greenbone/OpenVAS.

# Quick Start
* Start Kali VM
    * `vagrant up`
    * If it runs the provisioner, be aware that full configuration will take a long time, so be patient!
* Get a shell on the VM
    * `vagrant ssh`
* Optionally, set a new password for the Greenbone/OpenVAS admin user
    * `sudo runuser -u _gvm -- gvmd --user admin --new-password qwerty`
    * "qwerty" is now your new [terrible password](https://en.wikipedia.org/wiki/List_of_the_most_common_passwords)
* Start the Greenbone/OpenVAS service
    * `sudo gvm-start`
    * Per Kali standard, you must manually start this every time you restart Kali.
* Open the [Greenbone URL](https://localhost:9392)
    * There are two ways to access the web UI:
    * Open the Kali VM GUI, and within Kali open Firefox
    * Open an SSH connection, and forward SSH
        * `ssh -p $(vagrant ssh-config | grep Port | awk '{print $2}') -L 9392:localhost:9392 -i $(vagrant ssh-config | grep IdentityFile | awk '{print $2}') vagrant@localhost`
* Log in as user admin
    * Provide the password you received or set, as above

# Resources
By default, this VM is configured with 8 GB of memory and 4 cores.

If you want to reduce this, it is recommended to go no lower than 4 GB of
memory and 2 cores. If your scanner runs very slowly with this smaller
CPU/memory allocation, check your resource usage, and if necessary reduce your
NVTs and physical hosts.

# Updating
To update your definitions:
* Get a shell on the VM
* `sudo gvm-feed-update`

# Known issues

### failed to connect
Error:
`failed to connect to feed.community.greenbone.net`

If you tried to connect multiple times from one IP, Greenbone might have banned
you for the day. Try connecting from a different IP, or try again tomorrow!

### old VM image
Is there a newer kali rolling release vagrant box? As of 2021-01-29, there were
1000+ upgraded packages to download/install.

### no stable upgrade target
We install all "latest" packages, as opposed to specific targeted versions.
This makes it impossible to predict and lock down post-upgrade configuration
steps.

Possible solution: use dpkg configuration file to install targeted version,
instead of "latest".

### conditional reboot
After running `apt full-upgrade`, only reboot if required. This would require
VM introspection after running `01-provision-update.sh`. Vagrant may not
support this.

This is especially applicable if we manually run `vagrant provision`!

### slow scan
Check your available resources on your VM.

If your VM is stalling due to lack of CPU or memory, reduce your NVTs and
physical hosts.

# Ideas
* Automatically tune scanners based on available memory/cores
