#!/usr/bin/env bash
set -euo pipefail

apt -y autoremove
apt -y install greenbone-security-assistant openvas ufw

ufw allow 22
ufw allow 80
ufw allow 9392
ufw enable

pushd /home/vagrant/Documents
for repo in openvas_db_fix nikto_batch_scanner nmap_batcher; do
  [ -d $repo ] || git clone https://github.com/zerstoeren/$repo
done
popd

chown -R vagrant:vagrant /home/vagrant/Documents

set +e
gvm-setup > /var/log/gvm-setup.log
set -e

gvm-check-setup

echo
echo
grep -A1 'Please note the generated admin password' /var/log/gvm-setup.log || \
  echo "No new admin password found in log" && \
  echo "Presumably the admin user already exists" && \
  echo "See README.md if you want to reset your admin password"
